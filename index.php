<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Carousel Template for Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome 5 CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,900" rel="stylesheet"> 
    <!-- Custom styles for this template -->
    <style type="text/css">
    body{
      background-color: #fff;
      padding-top: 15%;
      text-align: center;
    }
    h2{
      display: inline-block;
    }
    </style>
  </head>
  <body>
<h1>Did you come for:</h1>
<a href="./rugby.php"><h2>Rugby</h2></a> or <a href="football.php"><h2>Football</h2></a>
  </body>
</html>
