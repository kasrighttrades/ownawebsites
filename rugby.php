<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>OWNA Rugby</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome 5 CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700,900" rel="stylesheet"> 
    <!-- Custom styles for this template -->
    <link href="./css/style-rg.css" rel="stylesheet">
  </head>
  <body>

<div class="backwhite">
  <header id="tiptop">
    <div class="quickcall">
      <div class="container social">
        <p><!--Call Now for a FREE Quote: 
          <a href="mailto:01942123456">
            <i class="fas fa-phone"></i> 01942 123456
          </a>-->
          <a href="#"><i class="fab fa-facebook"></i></a>
          <a href="#"><i class="fab fa-twitter"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
        </p>
      </div>
    </div>
    <div class="mainmenu" id="navhold">
      <nav class="navbar navbar-expand-md navbar-light" role="navigation">
          <div class="container">
              <a class="navbar-brand" href="#">
                <img src="./rsc/rg.png" class="md" alt="" />
                <img src="./rsc/rg-sm.png" class="sm" alt="" />
              </a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-bars"></i> 
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul id="n" class="navbar-nav ml-auto">
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-X current_page_item menu-item-X nav-item active"><a href="#tiptop" class="nav-link">Home</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-X nav-item"><a href="#lvideos" class="nav-link">Welcome</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-X nav-item"><a href="#lfeatures" class="nav-link">Features</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-X nav-item"><a href="#lscreenshots" class="nav-link">Screenshots</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-X nav-item"><a href="#lgift" class="nav-link">Download</a></li>
                </ul>
              </div>
          </div>
      </nav>
    </div>
    <div class="displayarea">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="welcomearea">
  <div class="showating">
  <h1>What's included when I book a consumer unit replacement?</h1>
  <p>RightTrades are approved by the NICEIC.</p>
  <button class="btn btn-primary paynow"><i class="fab fa-cc-stripe"></i> Buy Now</button>
  <button data-href="#lgift" class="btn btn-default jumpit"><i class="fas fa-mobile-alt"></i> Download App</button>
  </div>
  <div class="phone">
    <div class="phoneit">
      <div><img src="./rsc/app1.png" alt="" /></div>
      <div><img src="./rsc/app2.png" alt="" /></div>
      <div><img src="./rsc/app3.png" alt="" /></div>
      <div><img src="./rsc/app4.png" alt="" /></div>
      <div><img src="./rsc/app5.png" alt="" /></div>
    </div>
    <img src="./rsc/appback-rg.png" alt="" id="overlay"  />
  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>  
</div>


    <main role="main">

      <section id="lvideos" class="videos">
        <div class="container">
          <div class="row">
            <div class="col-md-1">&nbsp;</div>
            <div class="col-md-4">
              <iframe width="100%" height="280" src="https://www.youtube.com/embed/gvl4Zh4JxSQ?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-md-6">
              <h2>Welcome <span>to OWNA</span></h2>
              <p class="subt">sjioasd</p>
              <p>This is a description of the football or rugby site</p>
            </div>
            <div class="col-md-1">&nbsp;</div>
          </div>
        </div>
      </section>

      <section id="lfeatures" class="features">
        <div class="container">
          <div class="row">
          <div class="col-md-12">
            <h2>Feature <span>Bank</span></h2>
            <p class="subt">These are all the features associated with the app itself</p>
          </div>
            <div class="col-md-3">
<a href="#" class="feattip" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
<i class="fas fa-mobile-alt"></i>
<p>feat 1</p>
</a>
            </div>
            <div class="col-md-3">
<a href="#" class="feattip" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
<i class="fas fa-mobile-alt"></i>
<p>feat 2</p>
</a>
            </div>
            <div class="col-md-3">
<a href="#" class="feattip" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
<i class="fas fa-mobile-alt"></i>
<p>feat 3</p>
</a>
            </div>
            <div class="col-md-3">
<a href="#" class="feattip" title="<em>Tooltip</em> <u>with</u> <b>HTML</b>">
<i class="fas fa-mobile-alt"></i>
<p>feat 4</p>
</a>
            </div>
          </div>
        </div>
      </section>


      <section id="lscreenshots" class="screenshots">
        <div class="container">
          <div class="row">
          <div class="col-md-12">
            <h2>Screenshots <span>of the App in Motion</span></h2>
            <p class="subt">These are all the features associated with the app itself</p>
          </div>
          </div>
        </div>
        <div class="phonethatis">
          <div class="container">
            <div class="col-md-12">
<div class="phonethat">
  <div><img src="./rsc/app1.png" alt="" /></div>
  <div><img src="./rsc/app2.png" alt="" /></div>
  <div><img src="./rsc/app3.png" alt="" /></div>
  <div><img src="./rsc/app1.png" alt="" /></div>
  <div><img src="./rsc/app2.png" alt="" /></div>
  <div><img src="./rsc/app3.png" alt="" /></div>
  <div><img src="./rsc/app1.png" alt="" /></div>
  <div><img src="./rsc/app2.png" alt="" /></div>
  <div><img src="./rsc/app3.png" alt="" /></div>
</div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
          <div class="col-md-12">&nbsp;</div>
          </div>
        </div>
      </section>


      <section id="lgift" class="appasgift">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2>Buy this App <span>as a Gift to someone</span></h2>
              <p class="subt">These are all the features associated </p>
              <p>These are all the features associated with the app itself</p>
            </div>
            <!--<div class="col-md-2">
            </div>-->
            <div class="col-md-2">
<form action="/charge" method="POST" style="display:none;">
  <script
    src="https://checkout.stripe.com/checkout.js"
    class="stripe-button"
    data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"
    data-name="Demo Site"
    data-description="2 widgets ($20.00)"
    data-amount="2000">
  </script>
</form>
              <button class="btn btn-danger btn-default paynow">
                <i class="fab fa-cc-stripe"></i> Buy as Gift <b>via Stripe</b>
              </button>
            </div>
            <div class="col-md-2">
              <button class="btn  btn-primary btn-default">
                <i class="fab fa-google-play"></i> Download <b>GooglePLAY</b>
              </button>
            </div>
            <div class="col-md-2">
              <button class="btn  btn-primary btn-default">
                <i class="fab fa-apple"></i> Download <b>App Store</b>
              </button>
            </div>
          </div>
        </div>
      </section>

      <section id="lqa" class="qa">
        <div class="container">
          <div class="row">
          <div class="col-md-12">
            <h2>Questions <span>&amp; Answers</span></h2>
            <p class="subt">These are all the features associated with the app itself</p>
          </div>
          <div class="col-md-12">
<div class="accordion" id="accordionExample">

    <button id="headingOne" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      This is the amazing question that we have for the question block this is question #1
      <i class="fas fa-angle-double-down"></i>
    </button>
    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable.
    </div>

    <button id="headingTwo" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      This is the amazing question that we have for the question block this is question #2
      <i class="fas fa-angle-double-down"></i>
    </button>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable.
    </div>

    <button id="headingThree" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      This is the amazing question that we have for the question block this is question #3 
      <i class="fas fa-angle-double-down"></i>
    </button>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
        3Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable.
    </div>

</div>
            </div>
          </div>
        </div>
      </section>


      <section id="lcontact" class="contact">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2>Ask us <span>a Question</span></h2>
              <p class="subt">These are all the features associated with the app itself</p>
            </div>
            <div class="col-md-3">&nbsp;</div>
            <div class="col-md-6">
              <form>
                <input type="text" name="" id="" placeholder="Name">
                <input type="text" name="" id="" placeholder="Email">
                <input type="text" name="" id="" placeholder="Number">
                <textarea name="" id="" placeholder="Message"></textarea>
                <button class="btn btn-lg btn-primary">
                  <i class="fas fa-paper-plane"></i> 
                  Send Message
                </button>
              </form>
            </div>
            <div class="col-md-3">&nbsp;</div>
          </div>
        </div>
      </section>


    </main>

<footer>
    <div class="container">
      <div class="row">
        <div class="col-md-9 copyright">
          <p><a href="#">Privacy</a> &middot; <a href="#">Terms</a> &middot; <a href="#">Terms</a></p>
          <p id="cpr">&copy; 2017-2018 <b>OWNA Rugby</b></p>
        </div>
        <div class="col-md-3 social">
          <a href="#"><i class="fab fa-facebook"></i></a>
          <a href="#"><i class="fab fa-twitter"></i></a>
          <a href="#"><i class="fab fa-instagram"></i></a>
        </div>
      </div>
    </div>
</footer>


<button id="jumpup">
  <i class="fas fa-angle-up"></i>
</button>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./js/popper.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="./js/holder.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){

$('.feattip').tooltip({
  html: true
});

$('.phoneit').slick({
  autoplay: true,
  autoplaySpeed: 3000,
  infinite: true,
  dots: false,
  arrows: false,
  fade: true,
  cssEase: 'linear'
});


$('.phonethat').slick({
  autoplay: true,
  autoplaySpeed: 1000,
  infinite: true,
  dots: true,
  arrows: false,
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});

$(".paynow").click(function(){
  $(".stripe-button-el").click();
});
    

$(window).scroll(function(e){
  var h= $(this).scrollTop();
  var navis= $("header");
  if(h > 61){
    navis.addClass("stickit");
  }else{
    navis.removeClass("stickit");
  }
});

  $('#navbarNavDropdown .navbar-nav.ml-auto a,.jumpit').click(function(e){
    e.preventDefault();
    jumptoarea(this);
  });
  $("#jumpup").click(function(){
    $('#navbarNavDropdown .navbar-nav.ml-auto li').removeClass("active");
    $("html, body").animate({ scrollTop: "0px" });
    $('#navbarNavDropdown .navbar-nav.ml-auto li#top').addClass("active");
  });

    });

function jumptoarea(jumptoY){
  if($(jumptoY).parent()[0].localName=="li"){
    $('#navbarNavDropdown .navbar-nav.ml-auto li').removeClass("active");
    $(jumptoY).parent().addClass("active");
  }
  var getTab= 80;
  if(jumptoY.localName=="a"){
    getTab= $(jumptoY.hash)[0].offsetTop;
  }else{
    getTab= $(jumptoY.dataset.href)[0].offsetTop;
  }
  $("html, body").animate({ scrollTop: (parseInt(getTab)-80)+"px" });
}
    </script>
  </body>
</html>
